var express    = require('express');
var http       = require('http');
var mongodb    = require('mongodb');

//create app
var app = express();
app.get('/', function(req, res){
    res.send('Hello World');
});

//connect to database
var localhostMongoUri = 'mongodb://localhost:27017/eotw';
var mongoUri = process.env.MONGOLAB_URI || localhostMongoUri;
mongodb.MongoClient.connect(mongoUri, function(err, db) {
	if(err) {
		console.log("MongoDb Connection Error! "+err);
	}
	else {
		console.log("MongoDb Connected!");
	}
});

//start the server
var httpServer = http.createServer(app);
var port = process.env.PORT || 5000
httpServer.listen(port);
console.log("Server listening on port %d in %s mode", httpServer.address().port, app.settings.env);